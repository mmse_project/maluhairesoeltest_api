package com.MaluhaireSoeltestMMSE.MaluhaireSoeltestMMSEapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MMSEController {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    @GetMapping("/questions/{language}")
    public List<Question> getQuestion(@PathVariable String language) {
        List<Question> questions = jdbcTemplate.query("select * from Question where language =? order by order_num ASC",
                new Object[]{language}, (row, count) -> {
                    int id = row.getInt("id");
                    String questiontext = row.getString("questiontext");
                    String lang = row.getString("language");

                    Question question = new Question();
                    question.setId(id);
                    question.setQuestiontext(questiontext);
                    question.setLanguage(lang);

                    return question;
                }
        );

        for (Question question : questions) {
            question.setSubQuestions(getSubquestions(question.getId()));
        }

        return questions;
    }
// uus meetod /questions
    // kutsuda kaks korda välja getQuestion()
    // tulemus tagastada kombineeritud listina

    @GetMapping("/questions")
    public List<Question> getAllQuestions() {
        List<Question> questions = jdbcTemplate.query("select * from Question order by order_num ASC",
                new Object[]{}, (row, count) -> {
                    int id = row.getInt("id");
                    String questiontext = row.getString("questiontext");
                    String lan = row.getString("language");

                    Question question = new Question();
                    question.setId(id);
                    question.setQuestiontext(questiontext);
                    question.setLanguage(lan);

                    return question;
                }
        );
        // küsige alamküsimused
        // for loop...
        for (Question question : questions) {
            question.setSubQuestions(getSubquestions(question.getId()));
        }
        return questions;
    }

    private List<SubQuestion> getSubquestions(int parentQuestionId) {
        List<SubQuestion> subquestions = jdbcTemplate.query("select * from Subquestion where questionId = ?",
                new Object[]{parentQuestionId},
                (row, count) -> {
                    int id = row.getInt("id");
                    String subquestiontext = row.getString("subquestiontext");
                    String answer = row.getString("answer");
                    String image = row.getString("image");
                    int questionid = row.getInt("questionid");

                    SubQuestion subquestion = new SubQuestion();
                    subquestion.setId(id);
                    subquestion.setSubquestiontext(subquestiontext);
                    subquestion.setAnswer(answer);
                    subquestion.setImage(image);
                    subquestion.setQuestionId(id);
                    return subquestion;
                }
        );

        return subquestions;
    }

    @PutMapping("/question")
    public void editQuestion(@RequestBody Question question) {
        jdbcTemplate.update("update question set QuestionText = ? where id = ?", question.getQuestiontext(), question.getId());
    }

    @PutMapping("/subquestion")
    public void editSubQuestion(@RequestBody SubQuestion subquestion) {
        jdbcTemplate.update("update subquestion set SubQuestionText = ? where id = ?", subquestion.getSubquestiontext(), subquestion.getId());
    }}

