package com.MaluhaireSoeltestMMSE.MaluhaireSoeltestMMSEapi;

import java.util.List;

public class Question {
    private int id;
    private String questiontext;
    private List<SubQuestion> subQuestions;
    private String language;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestiontext() {
        return questiontext;
    }

    public String getLanguage() {
        return language;
    }


    public void setQuestiontext(String questiontext) {
        this.questiontext = questiontext;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public List<SubQuestion> getSubQuestions() {
        return subQuestions;
    }

    public void setSubQuestions(List<SubQuestion> subQuestions) {
        this.subQuestions = subQuestions;
    }
}
