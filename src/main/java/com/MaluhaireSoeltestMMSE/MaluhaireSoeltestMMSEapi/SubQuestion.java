package com.MaluhaireSoeltestMMSE.MaluhaireSoeltestMMSEapi;

public class SubQuestion {
    private int id;
    private String subquestiontext;
    private String answer;
    private int questionid;
    private String image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubquestiontext() {
        return subquestiontext;
    }

    public void setSubquestiontext(String subquestiontext) {
        this.subquestiontext = subquestiontext;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getQuestionId() {
        return questionid;
    }

    public void setQuestionId(int questionid) {
        this.questionid = questionid;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


}



