-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.12-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for mmsequery
DROP DATABASE IF EXISTS `mmsequery`;
CREATE DATABASE IF NOT EXISTS `mmsequery` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `mmsequery`;

-- Dumping structure for table mmsequery.question
DROP TABLE IF EXISTS `question`;
CREATE TABLE IF NOT EXISTS `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `QuestionText` varchar(500) NOT NULL,
  `Language` varchar(500) NOT NULL,
  `order_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- Dumping data for table mmsequery.question: ~21 rows (approximately)
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` (`id`, `QuestionText`, `Language`, `order_num`) VALUES
	(1, 'AJAS ORIENTEERUMINE', 'estonian', 1),
	(2, 'KOHAS ORIENTEERUMINE ', 'estonian', 2),
	(3, 'MEELDEJÄTMINE', 'estonian', 3),
	(4, 'TÄHELEPANU ja PEAST ARVUTAMINE', 'estonian', 4),
	(5, 'MEELDETULETAMINE', 'estonian', 5),
	(6, 'NIMETAMINE', 'estonian', 6),
	(7, 'KORDAMINE', 'estonian', 7),
	(8, 'OSKUSED', 'estonian', 8),
	(9, 'LUGEMINE', 'estonian', 9),
	(10, 'KIRJUTAMINE', 'estonian', 10),
	(11, 'KOPEERIMINE/KONSTRUEERIMINE', 'estonian', 11),
	(12, 'Ориентировка во времени', 'russian', 1),
	(13, 'Ориентировка в месте', 'russian', 2),
	(14, 'Запоминание', 'russian', 3),
	(15, 'Концентрация внимания и счет', 'russian', 4),
	(16, 'Память', 'russian', 5),
	(17, 'Речь', 'russian', 6),
	(18, 'Чтение', 'russian', 8),
	(19, 'Письмо', 'russian', 9),
	(20, 'Копирование', 'russian', 10),
	(21, 'Умения', 'russian', 7);
/*!40000 ALTER TABLE `question` ENABLE KEYS */;

-- Dumping structure for table mmsequery.subquestion
DROP TABLE IF EXISTS `subquestion`;
CREATE TABLE IF NOT EXISTS `subquestion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subquestiontext` varchar(500) NOT NULL,
  `answer` varchar(500) NOT NULL,
  `questionId` int(11) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `questionId` (`questionId`),
  CONSTRAINT `subquestion_ibfk_1` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- Dumping data for table mmsequery.subquestion: ~39 rows (approximately)
/*!40000 ALTER TABLE `subquestion` DISABLE KEYS */;
INSERT INTO `subquestion` (`id`, `subquestiontext`, `answer`, `questionId`, `image`) VALUES
	(1, 'Mis aasta praegu on?', '1', 1, NULL),
	(2, 'Mis kuu praegu on?', '1', 1, NULL),
	(3, 'Mis kuupäev praegu on?', '1', 1, NULL),
	(4, 'Mis nädalapäev täna on?', '1', 1, NULL),
	(5, 'Mis aastaaeg praegu on?', '1', 1, NULL),
	(6, 'Kus riigis me asume?', '1', 2, NULL),
	(7, 'Kus maakonnas me asume?', '1', 2, NULL),
	(8, 'Kus linnas (või asulas, külas) me praegu asume?', '1', 2, NULL),
	(9, 'Kus me praegu täpselt oleme? (haigla, tervisekeskuse nimetus, kodune aadress)', '1', 2, NULL),
	(10, 'Mitmendal korrusel me oleme?', '1', 2, NULL),
	(11, 'Ma ütlen Teile kolm sõna, jätke need palun meelde ja korrake neid, kui ma olen lõpetanud. PALL - AUTO - MEES. Anna üks punkt igale õigele vastusele. Üles märgitakse esimese korraga meelde jäänud sõnad. Kui esimeses kordamises esineb vigu, korratakse sõnu niikaua, kuni kõik kolm sõna on meelde jäänud (max 6 korda).  ', '3', 3, NULL),
	(12, 'Lahutage sajast seitse ja tulemusest jälle seitse ja jätkake seni, kuni palun Teil lõpetada. Katkestage tegevus peale viiendat vastust. (93, 86, 79, 72, 65). Paberit ja pliiatsit kasutada ei tohi. Alternatiiv: paluge öelda sõna "MAAILM" tagurpidi.', '5', 4, NULL),
	(13, 'Millised kolm sõna ma palusin Teil enne meelde jätta?  PALL - AUTO - MEES Sõnade järjekord pole tähtis. Jätke see küsimus vahele, kui testitav ei suutnud juba enne sõnu meelde jätta. Iga õigesti vastatud objekt annab ühe punkti.', '3', 5, NULL),
	(14, 'Mis see on? Näidake testitavale käekella.', '1', 6, NULL),
	(15, '\r\nMis see on? Näidake testitavale pliiatsit.', '1', 6, NULL),
	(16, 'Palun korrake täpselt minu järel: "Ei mingeid agasid ega kuisid." Lauset ei tohi korrata ja punkti saab vaid siis, kui lause on täiesti õige.', '1', 7, NULL),
	(17, 'Kolmeosalise korralduse täitmine: Ma annan Teile paberilehe. Võtke paber vasakusse/paremasse kätte (mis ei ole testitava dominantne käsi), murdke see keskelt pooleks ja asetage põrandale. Juhtnööre ei tohi korrata ega testitavat aidata. Iga sooritatud etapi eest saab 1 punkti. ', '3', 8, NULL),
	(18, 'Palun lugege ja täitke järgmine korraldus: "PALUN SULGEGE SILMAD" ', '1', 9, NULL),
	(19, 'Kirjutage siia paberilehele vastavalt oma soovile üks lühike lause. Punkti saab vaid juhul, kui lause on arusaadav, seal on vähemalt öeldis ja alus. Kirjavigu ei arvestata.', '1', 10, NULL),
	(20, 'Joonistage palun selle kujundi alla täpselt samasugune. Lubatud on mitu katset. Punkti saab vaid juhul, kui kõik küljed ja nurgad on olemas ning kahe viisnurga kattumisel moodustub ristkülik.', '1', 11, '/images/mental.png'),
	(21, 'Какое сегодя число?', '1', 12, NULL),
	(22, 'Какой сегодня месяц?', '1', 12, NULL),
	(23, 'Какой сейчас год?', '1', 12, NULL),
	(24, 'Какой сегодня день недели?', '1', 12, NULL),
	(25, 'Какое сейчас время года?', '1', 12, NULL),
	(26, 'В какой стране мы находимся?', '1', 13, NULL),
	(27, 'В какой области мы находимся?', '1', 13, NULL),
	(28, 'В каком городе мы находимся?', '1', 13, NULL),
	(29, 'Где мы в данный момент находимся? (Название больницы или медецинского центра, домашний адрес)', '1', 13, NULL),
	(30, 'На каком этаже мы находимся?', '1', 13, NULL),
	(32, 'Я назову вам три слова. Запомните и повторите следущее слова: Дом, Карандаш, Флаг', '3', 14, NULL),
	(33, 'Серийный счет. Пожалуйста, отнимите от 100 семь, от полученного результата еще раз отнимите семь, и так сделайте несколько раз. (93, 86, 79, 72, 65).', '5', 15, NULL),
	(34, 'Вспомните эти три слова, что вас просили запомнить:  Дом, Карандаш, Флаг', '3', 16, NULL),
	(35, 'Показываем ручку и часы, спрашиваем: "как это называется?" Каждый правильный ответ оценивается в один балл', '2', 17, NULL),
	(36, '3-х этапная команда. Устно дается команда, которая предусматривает последовательное совершение трех действий. "Возьмите лист бумаги правой рукой, сложите его вдвое и положите на стол. Каждое правильно выполненное действие оценивается в один балл.', '3', 21, NULL),
	(37, ' Пожалуйста прочтите и выполните следующее действие: Закройте глаза.Больной получает один балл, если после правильного прочтения вслух он действительно закрывает глаза.', '1', 18, NULL),
	(38, 'Пожалуйста, напишите на этом листке бумаги любое осмысленное и грамматически законченное предложение. Грамматические ошибки не учитываются.\r\n', '1', 19, NULL),
	(39, 'Пациенту дается образец (два пересекающихся пятиугольника с равными углами), который он должен перерисовать на нелинованной бумаге. Если при перерисовке возникают пространственные искажения или несоединение линий, выполнение команды считается неправильным.', '1', 20, '/images/mental.png'),
	(40, 'Просят больного повторить сложную в грамматическом отношении фразу, например: «Никаких если, и или но».', '1', 17, NULL);
/*!40000 ALTER TABLE `subquestion` ENABLE KEYS */;

-- Dumping structure for table mmsequery.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table mmsequery.user: ~1 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `password`) VALUES
	(1, 'admin', '$2a$10$OgHfbnzlNY05IEnJWV5VK.KQ.qdixKcVic9apU7kewS7QRyEXoSdq');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
